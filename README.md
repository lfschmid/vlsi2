# Content

 1. [Installation](#installation)
 2. [Update](#update)
 3. [Simulation](#simulation)
 5. [Netlist Extraction](#netlist-extraction)
 6. [Background Information](#background-information)
    1. [Timing](#timing)
    2. [Test Vector Generator](#test-vector-generator)


# Installation

 1. Change into your working direcotry (the one containing `cds.lib`).
 2. Checkout/download the new test module using `git clone https://tclgit.epfl.ch/lfschmid/vlsi2.git TESTER`
 3. Add the following line to `cds.lib`: `DEFINE TESTER TESTER`.
 4. Start Cadence Virtuoso as usual.
 5. Add your accumulator to the toplevel schematic `TOPLEVEL/schematic` in the new library `TESTER` and connect as shown on the following screenshot.
   * ![Screenshot](.doc/toplevel.png)
   * Terminate all unused ports with a `basic/noConn` element.
   * You don't need to add any sources or load capacitors as they are already included in the test module.
 6. Open the testbench `SIMULATION_TB/schematic` and select the clock you want to feed into your toplevel schematic (regular or inverted clock). Feed the same clock back into the testing module.
   * Terminate all unused ports with a `basic/noConn` element.
   * For a quick debugging you can use ideal sources (without a buffer and therefore unlimited driving strength), they contain the suffix `_ideal`. But please be aware that your final schematic simulation will be done based on the non-ideal sources.
   * If you have any issues verify you have understood the timing requirements as explained in the [Timing](#timing) section further below.


# Update
In case you have already checked out the testbench at an earlier moment the following command can be used to update your files within the `TESTER` folder:
`git pull`


# Simulation

 1. Open the simulation under `SIMULATION_TB/spectre_state1` in the new library `TESTER`.
 2. Optional: Add additional signals to be shown in your plot.
 3. Click on Netlist & Run to start the simulation.
 4. Enjoy our beautiful ASCII-Art.

 * The simulation runs at 1 GHz.
 * All outputs are driven by buffers with the following sizes: PMOS 2000nm/80nm, NMOS 800nm/8nm
 * All inputs have a capacitive load of 10fF.


# Netlist Extraction

 1. In your Virtuoso main window click on `File > Export > CDL`.
   * ![Screenshot](.doc/cdlout.png)
 2. Enter `TESTER/export_template` into the field `Template file` and press `Load...`.
   * ![Screenshot](.doc/export.png)
 3. Press `OK` at the bottom of the window.
 4. Upload your netlist file `netlist` located in your main directory to [moodle](http://moodle.epfl.ch/mod/assign/view.php?id=920584).


# Background Information

The `TestModuleWrapper` consists of a VerilogA module (`TestModule`) which reads test vectors bitwise from a vector file. At the same time it samples the outputs of your module and compares them to the precalculated values in the vector file. In case of an error the module aborts the simulation.

The VerilogA module prints its progress continuesly into the simulation output window. The final results or erros can also be found in the output file `output.txt`.

The vector file can be found in the same directory under the name `vectors.txt`. It consists of a 64bit binary sample followed by the expected 65bit output, one sample per line.

## Timing

It is crucial to understand the required timing of the test module. Concidering the following dataflow of your signal during simulation (see also the accumulator architecture provided in the [project specification](http://moodle.epfl.ch/mod/resource/view.php?id=884481) on moodle):
 1. Test Module Output
 2. Input DFF of Accumulator
 3. Adder of Accumulator
 4. Output DFF of Accumulator
 5. Test Module Input

As you can see the data is sampled twice, once at the input and once at the output of the accumulator. Therefore the test module will always see the effect of an applied input vector **2 periods** later.
The following timing diagram shows how the signal (in green) propagates through the five mentioned stages:
 * ![Screenshot](.doc/timing.png)
 * Please note that your design might differ from the timing diagram above.
 * Please pay attention that the setup time of the DFFs (stage 2 and 4) must be respected.
 * Furthermore no transition should occure at the input of the adder (stage 3) during the evaluation phase.

Some general remarks you should respect in order to ensure a correct functioning:
 * The output generation and sampling of the test module are snyced to the rising edge of its input clock.
 * Your devices need to be in the precharge phase during an active reset (meaning at the begining of the simulation):
   * Dynamic N-stage `Clk = 0`
   * Dynamic P-stage `Clk = 1`

## Test Vector Generator

Custom test vectors for more serious testing with more samples might be generated using the python script provided in the same directory under the name `gen_vector.py`. You can execute it using the following command in your terminal (in the same directory as this file) `python gen_vector.py`. It supports the following optional arguments:

    -h, --help
        Print this help text and exit
    -l, --length INT
        Amount of samples script should generate
    -f, --file FILENAME
        File to export vectors to

For example 100 samples might be created using `python gen_vector.py -l 100`.
