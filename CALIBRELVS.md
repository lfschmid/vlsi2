# LVS with Calibre
This manual shows how to set up LVS with calibre. We expect you to upload the report file from LVS together with your presentation for the exam.

 1. Start Calibre LVS from the layout window via `Calibre -> Run LVS`. A window pops up, asking for a runset. Close it and you end up with the Calibre LVS window.
 2. Click on the `Rules` button and set the LVS rules file to `RuleDecks/Calibre/LVS/G-DF-LOGIC_MIXED_MODE90N-1P9M-LOW_K_CALIBRE-LVS-1.2-P5.txt` ![Screenshot](.doc/calibre_rules.png)
 3. Click on `Inputs` and make sure `Export from layout viewer` is ticked for the `Layout` tab and `Export from schematic viewer` is ticked for the `Netlist` tab ![Screenshot](.doc/calibre_inputs.png)
 4. The report file expected to be uploaded to moodle can be set by clicking the `Outputs` button, most likely it is already reasonably named. ![Screenshot](.doc/calibre_outputs.png)
 5. LVS can by started by pressing the `Run LVS` button. If everything matches you are greeted with a smily ![Screenshoot](.doc/calibre_yeah_all_fine.png)
