%% Settings

%  Output File
output_file = '/home/lfschmid/VLSI2/vectors.txt';

%  Amount of Samples
amount_samples = 1000;

%  Resolution
resolution = 64;

%  Singed / Unsigned
signed = 0;

%% Generation

%  Set Seed
rnd_stream = RandStream('mt19937ar','Seed',1);
RandStream.setGlobalStream(rnd_stream);

%  Generate Number Stream
rand_nbrs = round(rand(amount_samples, 1) * (2^(resolution)-1)) - (signed == 1) * 2^(resolution-1);
if (signed == 1)
    rand_nbrs = int64(rand_nbrs);
else
    rand_nbrs = uint64(rand_nbrs);
end

max(rand_nbrs)
min(rand_nbrs)

%  Calculate corresponding Result
results = zeros(size(rand_nbrs));
results(1) = rand_nbrs(1);
for i=2:length(results)
    % overflow
    if (results(i-1) > (2^(resolution)-1) - (signed == 1) * 2^(resolution-1))
        tmp_result = results(i-1) - 2^(resolution - (signed == 1)*1);
    % underflow
    elseif (results(i-1) < 0 - (signed == 1) * 2^(resolution-1))
        tmp_result = results(i-1) + 2^(resolution - (signed == 1)*1);
    % normal
    else
        tmp_result = results(i-1);
    end
    
    % accumulation
    results(i) = tmp_result + rand_nbrs(i);
end

%% File Output

%  Unsigned
d = rand_nbrs;
[f,e]=log2(max(d)); % How many digits do we need to represent the numbers?
s=char(rem(floor(double(d)*pow2(1-max(resolution,e):0)),2)+'0');
