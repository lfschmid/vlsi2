# Simulation
This manual shows how to store all nets during simulation, allowing to add further nets to a plot afterwards without the need to rerun simulations. This also allows to find internal nets in automatically generated netlists (i.e. within extracted views).

 1. Run ADE L as usual
 2. Open `Outputs -> Save All` and tick `all` on the first line `Select signals to output` ![Screenshot](.doc/select_all_outputs.png)
 3. Run your simulation as usual.
 4. Change the visualisation window to `Browser` mode. ![Screenshot](.doc/set_to_browser_mode.png)
 5. The browser allows to navigate through the design, find the nets you are interested in and drag them into the simulation window. ![Screenshot](.doc/browse_design.png)

