#!/usr/bin/env python

import sys, getopt, random


def main(argv):

    resolution = 64
    length     = 10
    filename   = "vectors.txt"
    signed     = False
    debug      = False

    def help ():
        """print help about this script"""
        print("gen_vector - generate test and result vectors for your VLSI2 project")
        print("")
        print("    -h, --help")
        print("        Print this help text and exit")
        print("    -l, --length INT")
        print("        Amount of samples script should generate")
        print("    -f, --file FILENAME")
        print("        File to export vectors to")
        print("")
        print("    --resolution INT")
        print("        Resolution of generated numbers")
        print("    --signed")
        print("        Calculate results based on sigend numbers")
        print("    -d, --debug")
        print("        Create specific test vectors for verilogA testbench")
        print("")


    try:
        opts, args = getopt.getopt(argv, "h:l:f:d", ["help", "length=", "file=", "resolution=", "signed", "debug"])

    except getopt.GetoptError:
        help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            help()
            sys.exit()
        elif opt in ("-l", "--length"):
            length = int(arg)
        elif opt in ("-f", "--file"):
            filename = arg
        elif opt in ("--resolution"):
            resolution = int(arg)
        elif opt in ("--signed"):
            signed = True
        elif opt in ("-d", "--debug"):
            debug = True


    print( "### VLSI 2 - Test Set Generator ###" )

    print( "  Resolution:        {0}".format(resolution) )
    print( "  Amount of Samples: {0}".format(length) )
    print( "  Filename:          {0}".format(filename) )
    if not(signed):
        print("  Unsigned Numbers")
    if debug:
        print("  Debug File Generation")
    print("  Add --help to see how to change parameters above")

    print( "Generate Random Number..." )
    random.seed()

    inputs = []
    for i in range( 0, length ):
        inputs.append( random.getrandbits( resolution ) )

    if signed:
        inputs = [ x - 2**(resolution-1) for x in inputs ]


    print( "Calculate corresponding results..." )

    results = []
    results.append( inputs[0] )
    for i in range( 1, length ):
        if results[ i-1 ] > 2**(resolution - 1*(signed)) - 1:
            prev_result = results[ i-1 ] - 2**(resolution - 1*(signed))
        elif signed and results[ i-1 ] < -2**(resolution - 1):
            prev_result = results[ i-1 ] + 2**(resolution)
        else:
            prev_result = results[ i-1 ]

        results.append( prev_result + inputs[ i ])


    print( "Generate File..." )

    def negbin ( val, res ):
        """compute the 2's compliment of val"""
        if val < 0:
            val = 2**(res-1) + val
            val_str = "{0:b}".format( val, res-1 )
            return "1" + val_str.zfill( res-1 )
        else:
            val_str = "{0:b}".format( val, res )
            return val_str.zfill( res )

    f = open( filename, "w" )
    if debug:
        f.write( negbin( 0, resolution ) +" "+ negbin( inputs[0], resolution+1 ) +"\n" )
        f.write( negbin( 0, resolution ) +" "+ negbin( inputs[1], resolution+1 ) +"\n" )
        for i in range( 2, len(inputs) ):
            f.write( negbin( inputs[i-2], resolution ) +" "+ negbin( inputs[i], resolution+1 ) +"\n" )
    else:
        for i in range( len(inputs) ):
            f.write( negbin( inputs[i], resolution ) +" "+ negbin( results[i], resolution+1 ) +"\n" )
    f.close()

    print( "Done!" )


if __name__ == "__main__":
    main(sys.argv[1:])
